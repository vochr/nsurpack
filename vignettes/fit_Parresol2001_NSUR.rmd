---
title: "fitting Parresol 2001 by this NSUR package"
author: "christian.vonderach@mail.de"
date: "21 7 2020"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{NSUR Parresol (2001)}
  %\VignetteEngine{knitr::rmarkdown}
  \usepackage[utf8]{inputenc}
---

# Fitting a NSUR model

This vignette shows how to fit a nonlinear seemingly unrelated regression (NSUR)
as proposed by Parresol (2001) and the data provided therein.

The NSUR model fits multiple regression at once, optimising the errors of each
model at the same time, considering them 'seemingly unrelated' though the 
relation is explicitly included by a error covariance matrix.

Lets start checking the data of Parresol (2001).
```{r}
require(NSURpack)
data("SlashPine")
head(SlashPine)
```

## univariate Models
Following Parresol (2001), we fit univariate models using \code{nls()}, and 
check his error modelling. Finally, we compare our results to those in his paper.

### Wood: W ~ (b11 * (D^2 * H)^b12)
```{r, fig.align='center', fig.dim=c(7, 7)}
## Wood
## unweighted model
nls_wood <- nls(Wood ~ b11 * (D^2 * H)^b12, data=SlashPine, start = c(b11=0.1, b12=2))
plot(log(resid(nls_wood)^2) ~ log(SlashPine$D^2 * SlashPine$H))
abline(m <- lm(log(resid(nls_wood)^2) ~ log(SlashPine$D^2 * SlashPine$H)))
coef(m)[[2]] # identical to result of Parresol (2001, Tab.2): 2.114

## weighted model
nls_wood_w <- nls(Wood ~ b11 * (D^2 * H)^b12, data=SlashPine, start = c(b11=0.1, b12=2),
                  weights = 1 / (D^2 * H)^coef(m)[[2]])
summary(nls_wood_w)
coef(nls_wood_w) # Parresol (2001, Tab.2): 0.016363, 1.0585
res_wood_w <- resid(nls_wood_w) # keeping the weighted residuals
attr(res_wood_w, "df") <- df.residual(nls_wood_w) # and the degrees of freedom
summary(nls_wood_w)$sigma^2 # Parresol (2001, tab.2): 2.3446e-06
```
The results of the paper can fully be reconstructed.

### Bark B ~ (b21 * D^b22)
```{r, fig.align='center', fig.dim=c(7, 7)}
## Bark
## unweighted model
nls_bark <- nls(Bark ~ b21 * D^b22, data=SlashPine, start = c(b21=0.1, b22=2))
plot(log(resid(nls_bark)^2) ~ log(SlashPine$D))
abline(m <- lm(log(resid(nls_bark)^2) ~ log(SlashPine$D)))
coef(m)[[2]] # identical to result of Parresol (2001, Tab.2): 3.715

## weighted model
nls_bark_w <- nls(Bark ~ b21 * D^b22, data=SlashPine, start = c(b21=0.1, b22=2),
                  weights = 1/D^coef(m)[[2]])
summary(nls_bark_w)
coef(nls_bark_w) # Parresol (2001, Tab.2): 0.046277, 2.2093
res_bark_w <- resid(nls_bark_w) # keeping the weighted residuals
attr(res_bark_w, "df") <- df.residual(nls_bark_w) # and the degrees of freedom
summary(nls_bark_w)$sigma^2 # Parresol (2001, tab.2): 2.6609e-04
```

### Crown C ~ (b31 * D^b32 * H^b33)
```{r, fig.align='center', fig.dim=c(7, 7)}
## Crown
## unweighted model
nls_crown <- nls(Crown ~ b31 * D^b32 * H^b33, data=SlashPine, start = c(b31=0.1, b32=2, b33=1))
plot(log(resid(nls_crown)^2) ~ log(SlashPine$D))
abline(m <- lm(log(resid(nls_crown)^2) ~ log(SlashPine$D) + (I(SlashPine$H^2))))
coef(m)[2:3] # identical to result of Parresol (2001, Tab.2): 7.322, -0.006185

## weighted model
nls_crown_w <- nls(Crown ~ b31 * D^b32 * H^b33, data=SlashPine, start = c(b31=0.1, b32=2, b33=1),
                  weights = 1/(D^coef(m)[[2]] * exp(coef(m)[[3]] * H^2)))

summary(nls_crown_w)
coef(nls_crown_w) # Parresol (2001, Tab.2): 0.027378, 3.6804, -1.2624
res_crown_w <- resid(nls_crown_w) # keeping the weighted residuals
attr(res_crown_w, "df") <- df.residual(nls_crown_w) # and the degrees of freedom
summary(nls_crown_w)$sigma^2 # Parresol (2001, tab.2): 3.9417e-07
```

### Tree T ~ (b11 * (D^2 * H)^b12) + (b21 * D^b22) + (b31 * D^b32 * H^b33)
We don't need a separate unweighted regression model for the total aboveground
biomass, instead, we use the residuals of the unweighted component models.
```{r, fig.align='center', fig.dim=c(7, 7)}
## Tree
res_tree <- resid(nls_wood) + resid(nls_bark) + resid(nls_crown)
plot(log(res_tree^2) ~ log(SlashPine$D * SlashPine$H))
abline(m <- lm(log(res_tree^2) ~ log(SlashPine$D * SlashPine$H)))
coef(m)[[2]] # identical to result of Parresol (2001, eq. 19): 3.450

w_tree <- 1/(SlashPine$D * SlashPine$H)^coef(m)[[2]] # calculate weights
res_tree_w <- resid(nls_wood_w) + resid(nls_bark_w) + resid(nls_crown_w) # residuals
attr(res_tree_w, "df") <- 40 - 7 # degrees of freedom
```
All parameter estimates are pretty much ok; residual variance (sigma^2) slightly
differs from the results reported by Parresol (2001).

## system of equations: NSUR
```{r}
## setting up the NSUR-Model
## modeling-functions
wood_func <- Wood ~ b11 * (D^2 * H)^b12
bark_func <- Bark ~ b21 * D^b22
crown_func <- Crown ~ b31 * D^b32 * H^b33
tree_func <- Tree ~ (b11 * (D^2 * H)^b12) + (b21 * D^b22) + (b31 * D^b32 * H^b33)
model <- list(wood_func, bark_func, crown_func, tree_func)
labels <- sapply(model, function(m){as.character(m[[2]])})
## start values
startvals <- c(coef(nls_wood_w), coef(nls_bark_w), coef(nls_crown_w))
## weights matrix (p. 871, left column, far down)
weightsM <- list(weights(nls_wood_w), weights(nls_bark_w), weights(nls_crown_w), w_tree)
Delta <- buildWeightMatrix(weightsM, method = "SUR") #Weight-Matrix
## build error covariance matrix
resM <- list(res_wood_w, res_bark_w, res_crown_w, res_tree_w)
S <- buildSigma(resM, Delta) 
round(cov2cor(S), 4)
```

Now, having all parts of the model specified, we can optimise the regression
parameters using the functions implemented in this package.

```{r}
fit.sur <- nlsystemfit.sur(method = "SUR", eqns = model, #startvals = startvals,
                           startvals = c(b11=0.01, b12=1, b21=0.01, b22=2, b31=0.01, b32=2, b33=-1),
                           Delta = Delta, S = S, eqnlabels = labels, pl=1,
                           data=SlashPine)
```
Convergence code=2 means "successive iterates within tolerance, current iterate 
is probably solution".

We can compare to the results presented in Parresol (2001):
```{r}
rbind(fit.sur$b, 
      c(0.016245, 1.0590, 0.046040, 2.2112, 0.014015, 3.5098, -0.87190))
```

also the estimated standard errors are almost identical:
```{r}
rbind(fit.sur$se, 
      c(0.00172, 0.0124, 0.00530, 0.0397, 0.00436, 0.147, 0.193))
```

Checking the estimated variance-covariance matrix of the parameters
(check against p. 873, bottom):
```{r}
options(digits = 3, scipen = -2)
fit.sur$covb
options(digits = 7, scipen = 0)
fit.sur$sysvar # compare to 0.933 given on page 874, middle part
```

# prediction and confidence interval
```{r}
predict_sur(obj = fit.sur, newdata = data.frame(D=20, H=17), eqns = "Tree", se.fit = FALSE,
                        interval = "confidence", level = 0.95)
```
NB: prediction intervals cannot be calculated using the function, because it is
tight to close to the gnls approach (i.e. estimating variance and covariance 
using nlme::gnls) followed in Vonderach et al. (2018). 

## plotting results
```{r, fig.dim=c(7, 7)}
SlashPine$BHD <- SlashPine$D
plot_sur(obj = fit.sur, data = SlashPine, lmfor = F)
```


# Reference
Parresol, B. R. (2001). "Additivity of nonlinear biomass equations." Canadian Journal of Forest Research-Revue Canadienne De Recherche Forestiere 31(5): 865-878.
