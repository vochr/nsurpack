#' fit Parresol (2001)'s data
#'
#' $Id: Parresol_NSUR.r 667 2017-04-24 13:07:15Z Christian.Vonderach $

#### load libraries and functions ####
rm(list=ls())

#check if required packages are installed, in not, install
list.of.packages <- c("nlme","dplyr","tidyr","mice")
new.packages <-
  list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)>0) install.packages(new.packages)
#load required packages
invisible(lapply(list.of.packages, require, character.only = TRUE))

#load functions
mp <- "Programme/Eigenentwicklung"
source(file.path(getwd(), mp, "Models/SSFunctions", "SSBiomassFunc.r"))
source(file.path(getwd(), mp, "RCode/", "nlsystemfit_sur.r"))
source(file.path(getwd(), mp, "RCode/", "nlsystemfit_functions.r"))
source(file.path(getwd(), mp, "RCode/", "nlsystemfit_predict.r"))
source(file.path(getwd(), mp, "RCode/", "nlsystemfit_plot.r"))
source(file.path(getwd(), mp, "RCode/", "wknls.r"))
source(file.path(getwd(), mp, "RCode/", "pool_functions.r"))
source(file.path(getwd(), mp, "RCode/", "mice_EnvToDf.r"))

dat <- read.csv2(file.path("H:/FVA-Projekte/P01278_EnNa/P01278_EnNa_BiomassFunc/Daten/Urdaten/NSUR", "Parresol_2001_Tab_1.csv"))
Parresol2001 <- dat
save(SlashPine, file = file.path(getwd(), "data/SlashPine.rdata"))
## Wood (b11 * (D^2 * H)^b12)
nls_wood <- nls(Wood ~ b11 * (D^2 * H)^b12, data=dat, start = c(b11=0.1, b12=2))
plot(log(resid(nls_wood)^2) ~ log(dat$D^2 * dat$H))
abline(m <- lm(log(resid(nls_wood)^2) ~ log(dat$D^2 * dat$H)))
coef(m) # identical to resuls of Parresol
nls_wood_w <- nls(Wood ~ b11 * (D^2 * H)^b12, data=dat, start = c(b11=0.1, b12=2),
                  weights = 1 / (D^2 * H)^coef(m)[[2]])
res_wood_w <- resid(nls_wood_w)
attr(res_wood_w, "df") <- df.residual(nls_wood_w)

## Bark (b21 * D^b22)
nls_bark <- nls(Bark ~ b21 * D^b22, data=dat, start = c(b21=0.1, b22=2))
plot(log(resid(nls_bark)^2) ~ log(dat$D))
abline(m <- lm(log(resid(nls_bark)^2) ~ log(dat$D)))
coef(m) # identical to resuls of Parresol
nls_bark_w <- nls(Bark ~ b21 * D^b22, data=dat, start = c(b21=0.1, b22=2),
                  weights = 1/D^coef(m)[[2]])
res_bark_w <- resid(nls_bark_w)
attr(res_bark_w, "df") <- df.residual(nls_bark_w)

## Crown (b31 * D^b32 * H^b33)
nls_crown <- nls(Crown ~ b31 * D^b32 * H^b33, data=dat, start = c(b31=0.1, b32=2, b33=1))
plot(log(resid(nls_crown)^2) ~ log(dat$D))
abline(m <- lm(log(resid(nls_crown)^2) ~ log(dat$D) + (I(dat$H^2))))
coef(m)
nls_crown_w <- nls(Crown ~ b31 * D^b32 * H^b33, data=dat, start = c(b31=0.1, b32=2, b33=1),
                  weights = 1/(D^coef(m)[[2]] * exp(coef(m)[[3]] * H^2)))
res_crown_w <- resid(nls_crown_w)
attr(res_crown_w, "df") <- df.residual(nls_crown_w)

## Tree (b11 * (D^2 * H)^b12) + (b21 * D^b22) + (b31 * D^b32 * H^b33)
res_tree <- resid(nls_wood) + resid(nls_bark) + resid(nls_crown)
plot(log(res_tree^2) ~ log(dat$D * dat$H))
abline(m <- lm(log(res_tree^2) ~ log(dat$D * dat$H)))
coef(m) # identical to resuls of Parresol
w_tree <- 1/(dat$D * dat$H)^coef(m)[[2]]
res_tree_w <- resid(nls_wood_w) + resid(nls_bark_w) + resid(nls_crown_w)
attr(res_tree_w, "df") <- 40 - 7

################################################################################
##
## all parameter estimates pretty much ok; sigma^2 slightly different
##
################################################################################

## setting up the NSUR-Model
print("setting up the NSUR-Model")
## modeling-functions
wood_func <- Wood ~ b11 * (D^2 * H)^b12
bark_func <- Bark ~ b21 * D^b22
crown_func <- Crown ~ b31 * D^b32 * H^b33
tree_func <- Tree ~ (b11 * (D^2 * H)^b12) + (b21 * D^b22) + (b31 * D^b32 * H^b33)
##
model <- list(wood_func, bark_func, crown_func, tree_func)
labels <- sapply(model, function(m){as.character(m[[2]])})
startvals <- c(coef(nls_wood_w), coef(nls_bark_w), coef(nls_crown_w))
weightsM <- list(weights(nls_wood_w), weights(nls_bark_w), weights(nls_crown_w), w_tree)

Delta <- buildWeightMatrix(weightsM, method = "SUR") #Weight-Matrix
resM <- list(res_wood_w, res_bark_w, res_crown_w, res_tree_w)
S <- buildSigma(resM, Delta) # Sigma
round(cov2cor(S), 4)
fit.sur <- nlsystemfit.sur(method = "SUR", eqns = model, #startvals = startvals,
                           startvals = c(b11=0.01, b12=1, b21=0.01, b22=2, b31=0.01, b32=2, b33=-1),
                           Delta = Delta, S = S, eqnlabels = labels, pl=2,
                           data=dat)
dat$BHD <- dat$D
plot.sur(fit.sur, dat, F)

fit.sur$sysvar

### predict
obj <- fit.sur
newdata <- data.frame(D=20, H=17)
eqns <- c("Tree")
se.fit <- F
interval <- c("confidence", "prediction")
level<- 0.95

(EqNames <- sapply(1:4, function(x) obj$eq[[x]]$eq))
if("all" %in% eqns){
  eq <- 1:obj$g
  eqnames <- EqNames
} else {
  eq  <- which(EqNames %in% eqns)
  eqnames <- EqNames[eq]
}
if( length(eq) < 1) stop("'eqns' not contained in fitted object")

## make parameters available
## take them from pooled results!!!
for (i in 1:length(obj$b)) {
  name <- names(obj$b)[i]
  val <- obj$b[i]
  storage.mode(val) <- "double"
  assign(name, val)
}

attach(newdata)
Phi <- as.data.frame(matrix(0, nrow=nrow(newdata), ncol=length(eq)))
colnames(Phi) <- eqnames
Phi[, 1] <- (eval(parse(text=paste0("(D*H)^", 3.450)))) # tree variance model

## mean prediction
pred <- list()
(pred$mean <- as.vector(eval(as.formula(obj$eq[[ eq ]]$formula)[[3]])))
detach(newdata)

(df <- obj$eq[[eq]]$df) # number of oberservation in each component
(t <- qt(1-(1-level)/2, df = df))

## estimate confidence interval
fb <- as.matrix(attr(with(newdata, with(as.list(obj$b),
                                        eval(deriv(obj$eq[[ eq ]]$formula,
                                                   names(obj$b))))), "gradient"))
(estVar <- apply(fb, 1, function(a){t(a) %*% obj$covb %*% a}))


## total of predictions
pred$lwrCI <- pred$mean - (t * sqrt(estVar))
pred$uprCI <- pred$mean + (t * sqrt(estVar))

sii <- obj$rcov[eq,eq]
pred$lwdPI <- pred$mean - (t * sqrt(estVar + obj$sysvar * sii * Phi[, 1]))
pred$uprPI <- pred$mean + (t * sqrt(estVar + obj$sysvar * sii * Phi[, 1]))

print(pred)

