#' @title extract weight formula and calculate the weight
#' @description extract weight formula from gnls object and calculate the weight
#' @param obj a gnls object
#' @param data dataframe where to search for the variables needed to calculate
#' the weights
#' @details stop, if no implemented varFunc has been used and if the variables
#' needed are not found in 'data'
#' @return calculated weights, according to extracted formula and varFunc
#' @examples
#' 1+1
#' @export

extractWeight <- function(obj, data = parent.frame()){

  if(("gnls" %in% class(obj)) == TRUE){
    varModel <- obj$modelStruct$varStruct
  } else if("varM" %in% colnames(obj)){
    if("varFunc" %in% class(obj$varM) == TRUE) varModel <- obj$varM
  } else if("varFunc" %in% class(obj) == TRUE){
    varModel <- obj
  } else {
    stop("'obj' is not a 'gnls'-object, nor a 'varFunc'-object!")
  }

  vC <- class(varModel)[1]

  if(vC == "NULL"){

    expr <- parse(text=paste("rep(1, nrow(data))"))
    attach(data)
    w <- try(eval(expr), silent = TRUE)
    # attr(w, "formula") <- formula
    attr(w, "formula") <- attr(varModel, "formula")
    detach(data)

  } else if(vC == "varFixed"){ # ok for varFixed(~D) and weights = ~D | ~(D*H)^3.45

    formula <- as.formula(paste("y~", attr(varModel, "formula")[2],
                                sep=""))[[3]]
    w <- with(data, try(eval(formula), silent = T))
    attr(w, "formula") <- attr(varModel, "formula")

  } else if(vC == "varPower"){ # ok

    if(length(varModel) == 1){

      p <- varModel[1]
      formula <- as.formula(paste("y~", attr(varModel, "formula")[[2]],
                                  "^", p, sep=""))[[3]]
      w <- with(data, try(eval(formula), silent = T))
      attr(w, "formula") <- attr(varModel, "formula")

    } else if(length(varModel) > 1){

      group.name <- names(varModel)
      group.var <- as.character(attr(varModel, "formula")[[2]])[3]
      pred.var <- as.character(attr(varModel, "formula")[[2]])[2]
      df <- data[, c(pred.var, group.var)]
      w <- numeric(0)
      for(group in group.name){
        p <- varModel[[group]]
        formula <- as.formula(paste("y~", pred.var, "^", p, sep=""))[[3]]
        group.df <- subset(df, eval(parse(text=paste0(group.var, "=='", group, "'"))))

        attach(group.df)
        w <- c(w, try(eval(formula), silent = T))
        detach(group.df)
      }
      # attr(w, "formula") <- formula
      attr(w, "formula") <- attr(varModel, "formula")

    }

  } else if(vC == "varConstPower"){ # ok

    p1 <- varModel[[1]]
    p2 <- varModel[[2]]
    formula <- as.formula(paste("y~", p1, "+ abs(",
                                attr(varModel, "formula")[[2]],
                                ")^", p2, sep=""))[[3]]
    attach(data)
    w <- try(eval(formula), silent = T)
    # attr(w, "formula") <- formula
    attr(w, "formula") <- attr(varModel, "formula")
    detach(data)

  } else if(vC == "varExp"){

    p <- varModel[1]
    formula <- as.formula(paste("y~exp(", p, "*",
                                attr(varModel, "formula")[[2]],
                                ")", sep=""))[[3]]
    attach(data)
    w <- try(eval(formula), silent = T)
    # attr(w, "formula") <- formula
    attr(w, "formula") <- attr(varModel, "formula")
    detach(data)

  } else {

    stop(paste(vC, "not implemented!"))

  }

  if(class(w) == "try-error"){
    stop("in 'data' no variables found to calculate weights")
  }
  ## return weights as in gnls stored: attr(*gnls*$modelStruct$varStruct, "weights")
  return(1 / w)
}
