#' @title build weighted error covariance matrix
#' @description calculate the weighted error covariance matrix based on
#' nlme::gnls models
#' @param univGNLS list with univariate GNLS-fits for each system equation,
#' containing the residuals
#' @param Delta matrix containing the weights
#' @details calculate the weighted error covariance matrix based on
#' nlme::gnls models (cf. Parresol 2001, eq. 21)
#' Delta is an elementwise square root
#'    operation on the inverse of the weight matrix [Phi]',
#'    cf. Parresol (2001, p. 871)
#' @return invertible weighted error covariance matrix; if not invertible, NULL
#' @examples
#' 1+1
#' @export

buildECovMatrix <- function(univGNLS, Delta){
  G <- length(univGNLS)
  residi <- list()
  delta <- list()
  n <- array(0, c(G))
  k <- array(0, c(G))
  df <- array(0, c(G))
  beg <- array(0, c(G))
  end <- array(0, c(G))

  for (i in 1:G) {
    if("gnls" %in% class(univGNLS[[i]]) == TRUE){
      n[i] <- univGNLS[[i]]$dims$N
      k[i] <- univGNLS[[i]]$dims$p
      residi[[i]] <- univGNLS[[i]]$residuals ## weighted residuals
    } else if(attr(univGNLS[[i]]$Residuals, "label") == "Residuals"){
      n[i] <- univGNLS[[i]]$dims$N
      k[i] <- univGNLS[[i]]$dims$p
      residi[[i]] <- univGNLS[[i]]$Residuals ## weighted residuals
    }

    df[i] <- n[i] - k[i]
    beg[i] <- (i-1) * n[i] + 1
    end[i] <- i * n[i]

    delta[[i]] <- Delta[ beg[i]:end[i], beg[i]:end[i] ]
  }

  S <- Matrix(0, G, G)
  for (i in 1:G) {
    for (j in 1:G) {
      S[i, j] <- 1 / (sqrt((n[i] - k[i]) * (n[j] - k[j]))) *
        t(residi[[i]]) %*% (t(delta[[i]]) %*% delta[[j]]) %*% residi[[j]]
    }
  }

  ## if not of full rank, stop!
  if(rankMatrix(S, method = "qr") == G){ ## TRUE --> full rank!
    return(S)
  } else {
    stop("S (weighted error covariance matrix) not of full rank!")
    return(NULL)
  }
}
