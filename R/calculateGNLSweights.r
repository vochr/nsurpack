#' @title calculates regression weights from a vector of residuals
#' @description function calculates the weights from a vector of residuals by
#' using the methods of the nlme::gnls() function
#' @param res residuals from which the weights should be derived
#' @param weights Variance model to be evaluated; equivalent to weights-argument in gnls
#' @param dims number of parameter of model to which the residuals belong
#' @param data data.frame in which the variance covariates can be evaluated
#' @param plotTF if not NULL, characterstring specifying a variable in <data> which
#' should be to plot the weights against
#' @details NB: nlme-package models variance as var(e) = sigma^2 * |nu|^(2*delta) while
#' Parresol models the residuals as var(e) = sigma^2 * |nu|^delta, i.e. the
#' variance parameter delta is double of nlme-package
#' for direct usage the nlme-parameter has to be doubled!
#' the regression weights have to be squared, to reflect the doubled parameter!
#' this is done when the variance parameter is assigned to the object!!!
#' @return the weights
#' @examples
#' 1+1
#' @export

calculateGNLSweights <- function(res, weights, dims, data, plotTF=NULL){

  require(nlme) # should be installed anyway
  ## calculation of weights from residuals according to methods from nlme:::gnls()
  d <- list(p=dims, N=nrow(data), REML=FALSE)
  correlation <- NULL # here no correlation of residuals assumed; could be changed
  gnlsSt <- gnlsStruct(corStruct = NULL, varStruct = nlme:::varFunc(weights))
  attr(gnlsSt, "conLin") <-  list(Xy = array(res, c(nrow(data), 1)),
                                  dims = d, logLik = 0, sigma = 0)
  gnlsSt <- Initialize(gnlsSt, data) # --> attr(gnlsSt$varStruct, "weights") existiert
  optRes <- nlminb(c(coef(gnlsSt)), function(gnlsPars) -logLik(gnlsSt, gnlsPars))
  ##############################################################################
  ## NB: nlme-package models variance as var(e) = sigma^2 * |nu|^(2*delta) while
  ## Parresol models the residuals as var(e) = sigma^2 * |nu|^delta, i.e. the
  ## variance parameter delta is double of nlme-package
  ## for direct usage the nlme-parameter has to be doubled!
  ## the regression weights have to be squared, to reflect the doubled parameter!
  ## this is done when the variance parameter is assigned to the object!!!
  ##############################################################################
  #zuweisung des Parameters und Berechnung der weights mit übergebenem Parameter
  optRes$par <- 2 * optRes$par #parameter-Wert gedoppelt, vgl. nlme-Gleichung!
  coef(gnlsSt) <- optRes$par
  (vw <- attr(gnlsSt$varStruct, "weights")) # Extraktion der Weights
  if(!is.null(plotTF)) {
    oldpar <- par(no.readonly=TRUE)
    par(mfrow=c(1,1))
    grouplength <- length(unique(attr(gnlsSt$varStruct, "groups")))
    if(grouplength == 0){
      # plot(vw ~ data[, plotTF], pch=16, xlab=plotTF, main="weights")
      plot(log(res^2) ~ log(data[, plotTF]), main = "Variance-Function",
           ylab = "log(squared(residuals))", xlab = paste0("log(", plotTF, ")"),
           pch=16)
      abline(a=log(sqrt(sum((vw*res)^2)/(d$N - d$p))),
             b=coef(gnlsSt), lwd=2) ## original gnls_varFunc_parameter
      abline(lm(log(res^2) ~ log(data[, plotTF])), lwd=2, lty=2)
      legend("topleft", legend = c("nlme-package", "lm"), lwd=2, lty=c(1,2))
    } else {
      group <- c(1:length(unique(attr(gnlsSt$varStruct, "groups"))))
      names(group) <- unique(attr(gnlsSt$varStruct, "groups"))
      # plot(vw ~ data[, plotTF], pch=16, xlab=plotTF, main="weights",
      #      col = group[attr(gnlsSt$varStruct, "groups")])
      plot(log(res^2) ~ log(data[, plotTF]), main = "Variance-Function",
           ylab = "log(squared(residuals))", xlab = paste0("log(", plotTF, ")"),
           pch=16, col = 1+group[attr(gnlsSt$varStruct, "groups")])
      for(g in names(group)){
        abline(a=log(sqrt(sum((vw*res)^2)/(d$N - d$p))),
               b=coef(gnlsSt)[paste0("varStruct.", g)],
               col = 1+group[g], lwd=2)
      }
      abline(lm(log(res^2) ~ log(data[, plotTF])), lwd=2, lty=2)
      legend("topleft", legend = c("nlme-package", "lm"), lwd=2, lty=c(1,2))
    }
    par(oldpar)
  }
  names(optRes$par) <- gsub("varStruct.", replacement = "", x = names(optRes$par))
  attr(vw, "par") <- optRes$par
  return(vw)
}
