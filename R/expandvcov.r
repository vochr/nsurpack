#' @title expand vcov
#' @description expand vcov
#' @param q is ignored
#' @param u is returned
#' @details function taken from package 'mice' v2.22
#' @return parameter u, with no change
#' @examples
#' 1+1
#' @export

expandvcov <- function (q, u)
{
  err <- is.na(q)
  return(u)
}
