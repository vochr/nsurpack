#' @title plotting a fitted NSUR object
#' @description plotting different parts of a fitted NSUR object
#' @param obj fitted NSUR object
#' @param data data with which the NSUR model has been fitted
#' @param lmfor should mywhiskers as indicator for residuals plot be added (lmfor-type)?
#' @details this function plots several type of information
#' 1) observed and fitted vs. x-Axis (BHD)
#' 2) observed vs. fitted
#' 3) residuals vs. fitted
#' 4) std residuals vs. fitted
#' 5) qqnorm-plot
#' 6) applied weights during fitting
#' @return
#' @examples
#' 1+1
#' @importFrom lmfor mywhiskers
#' @export

plot_sur <- function(obj, data, lmfor=FALSE){

  if(lmfor == TRUE){
    if("lmfor" %in% installed.packages()){
      require("lmfor") # for function mywhiskers()
    } else {
      install.packages("lmfor")
      require("lmfor")
    }
  }

  oldpar <- par(no.readonly=TRUE)
  neqns <- length(obj$eq)
  plotpar <- function(l, orientation="u"){
    x <- floor(sqrt(l)):ceiling(sqrt(l))
    g <- expand.grid(x=x, y=x)
    g$d <- (g$x * g$y) - l
    if(!orientation %in% c("u", "r")){
      warning("orientation with wrong parameter; should be either 'u' or 'r'; set to 'u'")
      orientation <- "u"
    }
    if(orientation == "r"){
      val <- g[g$d>=0 & g$x<=g$y & g$d == min(g[g$d>=0,]$d), c("x", "y")]
    } else {
      val <- g[g$d>=0 & g$x>=g$y & g$d == min(g[g$d>=0,]$d), c("x", "y")]
    }
    return(c(val[[1]], val[[2]]))
  }
  plotdim <- plotpar(neqns)
  par(mfrow=plotdim, oma=c(3,3,0,0), mar=c(2,2,2,1))
  attach(data)
  estimate <- obj$b
  for (i in 1:length(estimate)) {
    name <- names(estimate)[i]
    assign(name, NULL)
    val <- estimate[i]
    storage.mode(val) <- "double"
    assign(name, val)
  }

  #### 1: fitted & observed vs. BHD ####
  for(i in 1:neqns){
    eqns <- obj$eq[[i]]$formula
    text <- as.character((eqns)[[2]])
    plot(eval(as.formula(eqns)[[2]])~BHD, pch=16, ylab=text, main=text,
         col=rgb(0, 0, 0, 0.5))
    points(x=data$BHD, y=obj$eq[[i]]$predicted, pch=3, col="red")
  }
  mtext(text="Bhd [cm]", side=1, line=1, outer=TRUE)
  mtext(text="Masse [kg]", side=2, line=1, outer=TRUE)
  detach(data)

  #### 2: fitted vs. observed ####
  par(mfrow=plotdim, oma=c(3,3,0,0), mar=c(2,2,2,1))
  for(i in 1:neqns){
    eqns <- obj$eq[[i]]$formula
    text <- as.character((eqns)[[2]])
    pred <- obj$eq[[i]]$predicted
    obs <- obj$eq[[i]]$residuals + obj$eq[[i]]$predicted
    plot(pred ~ obs, main=text, pch=16, col=rgb(0, 0, 0, 0.5))
    abline(0,1)
  }
  mtext(text="observed", side=1, line=1, outer=TRUE)
  mtext(text="fitted", side=2, line=1, outer=TRUE)

  #### 3: residuals vs. fitted ####
  par(mfrow=plotdim, oma=c(3,3,0,0), mar=c(2,2,2,1))
  for(i in 1:neqns){
    eqns <- obj$eq[[i]]$formula
    text <- as.character((eqns)[[2]])
    pred <- obj$eq[[i]]$predicted
    res <- obj$eq[[i]]$residuals
    plot(x = pred, y = res, main=paste("Residual Plot:", text), type="n")
    if(lmfor==TRUE){
      mywhiskers(pred, res, se = F, add=T)
      mywhiskers(pred, res, se = T, add=T, lwd=2)
    }
    panel.smooth(x = pred, y = res, col.smooth = "red", pch = 16,
                 col=rgb(0, 0, 0, 0.5))
    abline(0,0, lwd=1, col="black", lty=2)
  }
  mtext(text="fitted", side=1, line=1, outer=TRUE)
  mtext(text="residuals", side=2, line=1, outer=TRUE)

  #### 4: standardized residuals vs. fitted ####
  ## standardized residual_i = residual_i / (std_dev of Residual_i)
  par(mfrow=plotdim, oma=c(3,3,0,0), mar=c(2,2,2,1))
  varW <- varWeights(obj)
  for(i in 1:neqns){
    eqns <- obj$eq[[i]]$formula
    text <- as.character((eqns)[[2]])
    pred <- obj$eq[[i]]$predicted
    res <- as.vector(obj$eq[[i]]$residuals)
    ## we need to devide by the std_deviation not the variance!
    ## hence, take the sqrt of the estimated variance!
    w <- sqrt(1/varW[[i]])
    n <- obj$eq[[i]]$n
    p <- obj$eq[[i]]$k
    sigma <- sqrt(sum((w * res)^2) / (n - p)) ## see nlme:::gnls, line 340
    std <- sigma / w ## see nlme:::gnls, line 355
    stdres <- res / std ## see nlme:::residuals.gnls, line 7
    plot(x = pred, y = stdres,
         main=paste("Std. Res.:", text), type="n")
    if(lmfor==TRUE){
      mywhiskers(pred, stdres, se = F, add=T)
      mywhiskers(pred, stdres, se = T, add=T, lwd=2)
    }
    panel.smooth(x = pred, y = stdres, col.smooth = "red", pch = 16,
                 col=rgb(0, 0, 0, 0.5))
    abline(0,0, lwd=1, col="black", lty=2)
  }
  mtext(text="fitted", side=1, line=1, outer=TRUE)
  mtext(text="standardized residuals", side=2, line=1, outer=TRUE)

  #### 5: qqnorm ####
  ## standardized residual_i = residual_i / (std_dev of Residual_i)
  par(mfrow=plotdim, oma=c(3,3,0,0), mar=c(2,2,2,1))
  varW <- varWeights(obj)
  for(i in 1:neqns){
    eqns <- obj$eq[[i]]$formula
    text <- as.character((eqns)[[2]])
    pred <- obj$eq[[i]]$predicted
    res <- as.vector(obj$eq[[i]]$residuals)
    ## we need to devide by the std_deviation not the variance!
    ## hence, take the sqrt of the estimated variance!
    w <- sqrt(1/varW[[i]])
    n <- obj$eq[[i]]$n
    p <- obj$eq[[i]]$k
    sigma <- sqrt(sum((w * res)^2) / (n - p)) ## see nlme:::gnls, line 340
    std <- sigma / w ## see nlme:::gnls, line 355
    stdres <- res / std ## see nlme:::residuals.gnls, line 7
    qqnorm(stdres, datax=TRUE, main = paste("Normal-QQ-Plot:", text), pch=16,
           col=rgb(0, 0, 0, 0.5))
    qqline(stdres, datax = TRUE)
  }
  mtext(text="Sample Quantiles", side=1, line=1, outer=TRUE)
  mtext(text="Theoretical Quantiles", side=2, line=1, outer=TRUE)

  #### 6: normality assumption ####
  ## standardized residual_i = residual_i / (std_dev of Residual_i)
  par(mfrow=plotdim, oma=c(3,3,0,0), mar=c(2,2,2,1))
  varW <- varWeights(obj)
  for(i in 1:neqns){
    eqns <- obj$eq[[i]]$formula
    text <- as.character((eqns)[[2]])
    pred <- obj$eq[[i]]$predicted
    res <- as.vector(obj$eq[[i]]$residuals)
    ## we need to devide by the std_deviation not the variance!
    ## hence, take the sqrt of the estimated variance!
    w <- sqrt(1/varW[[i]])
    n <- obj$eq[[i]]$n
    p <- obj$eq[[i]]$k
    sigma <- sqrt(sum((w * res)^2) / (n - p)) ## see nlme:::gnls, line 340
    std <- sigma / w ## see nlme:::gnls, line 355
    stdres <- res / std ## see nlme:::residuals.gnls, line 7
    normal <- ks.test(x = stdres, y = "pnorm", alternative = "two.sided")
    hist(stdres, freq = F, main=paste("KS-Normality-Test:", text),
         col=ifelse(normal$p.value>0.05, "blue", "red"))
    lines(density(stdres), lwd=2)
    curve(dnorm(x, 0, 1), add=T, lwd=2, lty=2)
    legend("topright", legend=c("data", "normal", "normal", "not normal"),
           lwd=c(2, 2, NA, NA), lty=c(1, 2, NA, NA), pch=c(NA, NA, 22, 22),
           col=c(1, 1, "blue", "red"), pt.bg = c(NA, NA, "blue", "red"), pt.cex=2)
  }
  mtext(text="Standardized Residuals", side=1, line=1, outer=TRUE)
  mtext(text="Density", side=2, line=1, outer=TRUE)

  #### 7: weights-functions ####
  if(!is.null(obj$eq[[1]]$varModel)){
    par(mfrow=plotdim, oma=c(3,3,0,0), mar=c(2,2,2,1))
    Delta <- obj$weights
    for(i in 1:neqns){
      varFunc <- as.character(obj$eq[[i]]$varModel)[[2]]
      (varCovar <- strsplit(varFunc, split = " | ", fixed = T)[[1]][1])
      text <- as.character((obj$eq[[i]]$formula)[[2]])
      plot(attr(Delta, paste0("weights:eqn", i)) ~ data[, varCovar],
           main=paste("applied weights:", text),
           pch=16, col=rgb(0, 0, 0, 0.5))
    }
    mtext(text="variance covariate", side=1, line=1, outer=TRUE)
    mtext(text="weight", side=2, line=1, outer=TRUE)
  }

  #### 8: variance-functions ####
  if(!is.null(obj$eq[[1]]$varModel)){
    par(mfrow=plotdim, oma=c(3,3,0,0), mar=c(2,2,2,1))
    varW <- varWeights(obj)
    for(i in 1:neqns){
      varFunc <- as.character(obj$eq[[i]]$varModel)[[2]]
      (varCovar <- strsplit(varFunc, split = " | ", fixed = T)[[1]][1])
      text <- as.character((obj$eq[[i]]$formula)[[2]])
      logsquaredRes <- log(as.vector(obj$eq[[i]]$residuals)^2)
      ## we need to divide by the std_deviation not the variance!
      ## hence, take the sqrt of the estimated variance!
      w <- sqrt(1/varW[[i]])
      res <- as.vector(obj$eq[[i]]$residuals)
      n <- obj$eq[[i]]$n
      p <- obj$eq[[i]]$k
      (sigma <- sqrt(sum((w * res)^2) / (n - p))) ## see nlme:::gnls, line 340
      ### sigma for nls-model: from summary.nls
      # dev <- (sum((nls_stori_w$m$resid())^2))
      # dev <- deviance(nls_stori_w)
      # sigma <- sqrt(dev/(n - p))
      plot(logsquaredRes ~ log(data[, varCovar]),
           main=paste("variance function:", text),
           pch=16, col=rgb(0, 0, 0, 0.5))
      vPar <- obj$eq[[i]]$varPar
      if(length(vPar) > 1){
        col = 1
        for(v in vPar){
          col = col + 1
          abline(a=log(sigma^2), b=v, col=col)
        }
      } else {
        abline(a=log(sigma^2), b=vPar)
      }
      abline(lm(logsquaredRes ~ log(data[, varCovar])), lwd=2, lty=2)
      legend("topleft", legend = c("nlme-package", "lm"), lwd=c(1,2), lty=c(1,2))
    }
    mtext(text="log(varCovar)", side=1, line=1, outer=TRUE)
    mtext(text="log(squared(residuals))", side=2, line=1, outer=TRUE)
  }

  #### 9: print parameters ####
  par(mfrow=plotdim)
  t <- array(data=c(obj$b, obj$se, obj$t, obj$p), c(length(obj$b), 4),
             list(names(obj$b), c("Value", "Std.Error", "t-value", "p-value")))
  for(i in 1:neqns){
    text <- obj$eq[[i]]$eqnlabel
    ## check if current equation is a summary compartment (only valid for biomass functions)
    ## check by testing whether all parameters have the same first digit as this
    ## is kind of a convention here
    vars <- all.vars(obj$eq[[i]]$formula)
    vars2 <- vars[grep(pattern = "[a-z][[:digit:]]", vars)]
    x <- sapply(strsplit(vars2, ""), function(x, n, m) tail(head(x, n), m), n=2, m=1)
    use <- length(unique(x)) == 1
    if(use == T | i == neqns){ ## last summary compartment is used to give Sigma
      if(i != neqns){
        st <- t[which(substr(rownames(t), 2, 2) == unique(x)), c(1, 2)]
        plotlabel <- paste("parameter estimates: ", text)
      } else {
        st <- round(cov2cor(obj$rcovest), 2)
        # covnames <- sapply(1:length(obj$eq), function(a) obj$eq[[a]]$eqnlabel)
        covnames <- paste0("c", 1:length(obj$eq))
        st <- array(st, dim = st@Dim, dimnames = list(covnames, covnames))
        plotlabel <- "correlation matrix"
      }

      plot(1, type="n", main = plotlabel, xaxt='n', yaxt='n', xlab="", ylab="",
           xlim=c(0,1), ylim=c(0,1))

      n <- (nrow(st)+1)
      m <- (ncol(st)+1)
      for(j in 1:n){
        for(k in 1:m){
          if(j == 1){
            if(k == 1){
              # j == 1, k == 1
              text(0+(1/(m+1))*k, 1-(1/(n+1))*j, labels = "")
            } else {
              # j == 1, k != 1
              text(0+(1/(m+1))*k, 1-(1/(n+1))*j, labels = attr(st, "dimnames")[[2]][k-1])
            }
          } else {
            if(k == 1){
              # j != 1, k == 1
              text(0+(1/(m+1))*k, 1-(1/(n+1))*j, labels = attr(st, "dimnames")[[1]][j-1])
            } else {
              # j != 1, k != 1
              text(0+(1/(m+1))*k, 1-(1/(n+1))*j, labels = round(st[j-1,k-1], 6))
            }
          }
        }
      }
    }
  }
  par(oldpar)
}
