#' Slash Pine data from Parresol (2001)
#'
#' This data is taken from the Parresol (2001) publication, with which the
#' described method was illustrated and proven.

#' @format A data frame with 40 observations on the following 9 variables.
#'   \describe{
#'     \item{\code{TreeNo}}{tree identifier}
#'     \item{\code{D}}{diameter at breast height, cm}
#'     \item{\code{H}}{height, m}
#'     \item{\code{LCL}}{Live crown length, m}
#'     \item{\code{Age}}{tree age, years}
#'     \item{\code{Wood}}{green mass of wood, kg}
#'     \item{\code{Bark}}{green mass of bark, kg}
#'     \item{\code{Crown}}{green mass of crown, kg}
#'     \item{\code{Tree}}{green mass of total tree, kg}
#'   }
#' @source Parresol (2001)
#' @references Parresol, B. R. (2001). "Additivity of nonlinear biomass equations."
#' Canadian Journal of Forest Research-Revue Canadienne De Recherche Forestiere 31(5): 865-878.
#' @examples
#' data(SlashPine)
#' @keywords datasets
"SlashPine"

