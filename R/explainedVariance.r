#' @title extract explained variance of model
#' @description calculate the share of explained variance for each element
#' of the model (fixed part, random part, residual variance, total variance)
#' @param finalMod model to extract fixed and resid part
#' @param adjMod model which has been used to adjust the data
#' @details function to calculate the share of explained variance for each element
#' of the model (fixed part, random part, residual variance, total variance).
#' In case of a model sequence with random effects-adjustment, this part of
#' variation is also included.
#' structure assumed is:
#' total = original data (orig)
#' total = adjusted data (adj) + random effects adjustment (ranef)
#' total = (fixed + residual) + random effects adjustment
#' in terms of variance, this structures as follows:
#' total = var(orig)
#' var(orig) = var(adj) + var(ranef) + 2*cov(adj, ranef)
#' var(orig) = var(fixed + resid) + var(ranef) + 2*cov(adj, ranef)
#' var(orig) = (var(fixed) + var(resid) + 2*cov(fixed, resid)) +
#'                                             var(ranef) + 2*cov(adj, ranef)
#' @return res matrix keeping the information about the explained variance of
#' the different model terms: total: total variance of data, adj:variance
#' of random effect adjusted data, fixed: variance explained by the fixed part
#' of the model, random: variance explained by the random part of the model,
#' residual: residual variance of the model, cov: covariance between the named
#' vectors, variance_removed: variance removed during adjustmend of data
#' @examples
#' 1+1
#' @export
#'

explainedVariance <- function(finalMod, adjMod = NULL){

  if("gnls" %in% class(finalMod)){
    if(!is.null(adjMod) & "nlme" %in% class(adjMod)){
      # case, where finalMod (i.e. fixed + resid part) and adjMod is available
      orig <- adjMod$fitted[, "fixed"] + adjMod$residuals[, "fixed"]
      total_var <- var(orig)
      adj <- finalMod$fitted + finalMod$residuals
      adj_var <- var(adj)
      ranef <- orig - adj
      ranef_var <- var(ranef)
      fixed <- finalMod$fitted
      fixed_var <- var(fixed)
      resid <- finalMod$residuals
      resid_var <- var(resid)
      total_var_hat <- (fixed_var + resid_var + 2*cov(fixed, resid)) +
        ranef_var + 2*cov(adj, ranef)
      res <- matrix(NA, nrow = 8, ncol = 2,
                    dimnames = list(c("total", "adj", "fixed", "random",
                                      "residual", "cov(fix, res)", "cov(adj, ran)",
                                      "variance removed"),
                                    c("Var", "Var_%")))
      res[, 1] <- c(total_var, adj_var, fixed_var, ranef_var, resid_var,
                    cov(fixed, resid), cov(adj, ranef), ranef_var+2*cov(adj, ranef))
      res[, 2] <- round(res[, 1] * c(rep(1, 5), 2, 2, 1) / total_var * 100, 2)

      if(total_var - total_var_hat<0.001){
        print(paste0(round((ranef_var+2*cov(adj, ranef)) / total_var * 100, 2),
                     "% of variance removed due to random effects adjustment!"))
      } else {
        print("Error in variance calculation!")
      }

    } else {
      # case, where only finalMod (i.e. fixed + resid part) is available
      orig <- finalMod$fitted + finalMod$residuals
      total_var <- var(orig)
      fixed <- finalMod$fitted
      fixed_var <- var(fixed)
      resid <- finalMod$residuals
      resid_var <- var(resid)
      total_var_hat <- (fixed_var + resid_var + 2*cov(fixed, resid))

      res <- matrix(NA, nrow = 4, ncol = 2,
                    dimnames = list(c("total", "fixed", "residual", "cov(fix, res)"),
                                    c("Var", "Var_%")))
      res[, 1] <- c(total_var, fixed_var, resid_var, cov(fixed, resid))
      res[, 2] <- round(res[, 1] * c(rep(1, 3), 2) / total_var * 100, 2)

      if(total_var - total_var_hat<0.001){
        print(paste0("No Variance due to random effects adjustment removed!"))
      } else {
        print("Error in variance calculation!")
      }
    }
  }
  return(res)
}
