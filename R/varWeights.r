#' @title extract varweights
#' @description extract varweights of fitted NSUR object
#' @param varObj fitted NSUR-object
#' @details no details
#' @return a list
#' @examples
#' 1+1
#' @import Matrix
#' @export

varWeights <- function(varObj){
  require("Matrix")
  Delta <- varObj$weights
  VarWeights <- diag(solve(Delta)^2)
  eqVarWeights <- list()
  l <- 0
  u <- 1
  for(i in 1:length(varObj$eq)){
    n <- varObj$eq[[i]]$n
    eqVarWeights[[i]] <- VarWeights[(l*n+1):(u*n)]
    l <- l+1
    u <- u+1
  }
  return(eqVarWeights)
}
