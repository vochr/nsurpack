#' @title working function to be optimised
#' @description this is the regression setting to fit the NSUR-model
#' @param theta parameter vector
#' @param eqns equations
#' @param data to be fitted
#' @param fitmethod either "OLS" or "SUR"
#' @param parmnames parameter names
#' @param instr not used
#' @param S Sigma
#' @param W weights
#' @param useHessian for optimisation
#' @details copy from the systemfit:::knls, extended to incorporate weights,
#' see also Parresol (2001)
#' @return a matrix?
#' @examples
#' 1+1
#' @export

wknls <- function (theta, eqns, data, fitmethod = "OLS",  parmnames,
                  instr = NULL, S = NULL, W = NULL, useHessian = FALSE)
{
  r <- matrix()
  r <- NULL
  gmm.resids <- matrix()
  gmm.resids <- NULL
  residi <- list()
  lhs <- list()
  rhs <- list()
  neqs <- length(eqns)
  nobs <- dim(data)[[1]]
  moments <- list()
  mn <- array()
  moments <- NULL
  mn <- NULL
  lhs <- NULL
  rhs <- NULL
  residi <- NULL
  dResidTheta <- NULL
  dResidThetai <- list()
  d2ResidTheta <- array(NA, c(0, 0, 0))
  d2ResidThetai <- list()
  for (i in 1:length(parmnames)) {
    name <- names(parmnames)[i]
    val <- theta[i]
    storage.mode(val) <- "double"
    assign(name, val)
  }
  for (i in 1:length(eqns)) {
    lhs[[i]] <- as.matrix(eval(as.formula(eqns[[i]])[[2]]))
    rhs[[i]] <- as.matrix(eval(as.formula(eqns[[i]])[[3]]))
    residi[[i]] <- lhs[[i]] - rhs[[i]]
    r <- rbind(r, as.matrix(residi[[i]]))
    if (fitmethod == "GMM") {
      gmm.resids <- cbind(gmm.resids, as.matrix(residi[[i]]))
    }
    dResidThetai[[i]] <- -attributes(with(data, with(as.list(theta),
                            eval(deriv(eqns[[i]], names(parmnames))))))$gradient
    dResidTheta <- rbind(dResidTheta, dResidThetai[[i]])
    d2ResidThetai[[i]] <- -attributes(with(data, with(as.list(theta),
                            eval(deriv3(eqns[[i]], names(parmnames))))))$hessian
    temp <- array(NA, c(dim(d2ResidTheta)[1] + dim(d2ResidThetai[[i]])[1],
                        dim(d2ResidThetai[[i]])[2:3]))
    if (i > 1) {
      temp[1:dim(d2ResidTheta)[1], , ] <- d2ResidTheta
    }
    temp[(dim(d2ResidTheta)[1] + 1):(dim(temp)[1]), , ] <- d2ResidThetai[[i]]
    d2ResidTheta <- temp
  }

  if (fitmethod == "SUR") {
    R <- Matrix(r) ## Residuals
    if(is.null(W)){ ## without weights 'W'
      ## https://en.wikipedia.org/wiki/Gauss%E2%80%93Newton_algorithm#Derivation_from_Newton.27s_method
      ## beta^(k+1) = beta^(k) - H^(-1)g
      ## g = 2*t(J_r) r
      ## H =approx 2*t(J_r)J_r
      ## wobei J = jacobian; r = residuals; H = hessian; k = iteration
      obj <- crossprod(t(crossprod(R, S)), R)[1,1]
      jac <- 2 * (t(R) %*% S %*% dResidTheta) # gradient
      if(useHessian == TRUE){
        hess <- 2 * (t(dResidTheta) %*% S %*% dResidTheta) # hessian
        attributes(obj) <- list(gradient = jac[1,], hessian = as.matrix(hess))
      } else {
        attributes(obj) <- list(gradient = jac[1,])
      }
    } else { ## with weights 'W'
      obj <- crossprod(t(crossprod(R, (t(W) %*% S %*% W))), R)[1,1]
      jac <- 2 * (t(R) %*% (t(W) %*% S %*% W) %*% dResidTheta) # gradient

      if(useHessian==TRUE){
        hess <- 2 * t(dResidTheta) %*% (t(W) %*% S %*% W) %*% dResidTheta # approximated hessian
        attributes(obj) <- list(gradient = jac[1,], hessian = as.matrix(hess))
      } else {
        attributes(obj) <- list(gradient = jac[1,])
      }
    }
  }
  return(obj)
}
