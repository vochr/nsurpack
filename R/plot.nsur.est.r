#' @title plotting obs and est
#' @description plotting the observations and estimates from a fitted mira
#' object for each imputed data set
#' @param fit a 'mira' object with fitted nlsystemfit result
#' @param imp a 'mids' object, result from imputation via 'mice()'
#' @param new.vars list containing a formula, where the lhs is considered
#' to be a new variable name and the rhs to be the describtion on  how to
#' calculate the new variable. All necessary data must be contained in
#' complete(imp, 1); Example: new.vars = list(formula(KL ~ Hoehe - KRO))
#' @details no details
#' @return side-effects: plot
#' @examples
#' 1+1
#' @import mice
#' @export

plot_nsur_est <- function(fit, imp, new.vars=NULL){


  Cols <- sapply(1:length(fit$analyses[[1]]$eq), function(a){
    fit$analyses[[1]]$eq[[a]]$eqnlabel
  })
  maxComp <- sapply(1:10, function(a) apply(complete(imp, a)[, Cols], 2, max))
  maxComp <- apply(maxComp, 1, max)
  for(i in 1:length(fit$analyses)){
    par(mfrow=c(3,4))
    G  <- length(fit$analyses[[i]]$eq)
    labels <- sapply(1:G, function(a) fit$analyses[[i]]$eq[[a]]$eqnlabel)
    dat <- complete(imp, i)
    if(!is.null(new.vars)){
      for(j in 1:length(new.vars)){
        var <- as.character(new.vars[[j]][[2]])
        rhs <- new.vars[[j]][[3]]
        dat[, var] <- with(dat, eval(rhs))
      }
    }
    pred <- predict_sur(obj = fit$analyses[[i]], newdata=dat, eqns=c("all"),
                        se.fit = FALSE, interval = c("none"), level=0.95)
    ## plot results for first imputed data set!
    for(j in 1:G){
      plot(dat[, labels[[j]] ] ~ dat$BHD, col="red", pch=19, xlab="BHD [cm]",
           main=paste0("mids ", i, ": ", labels[[j]]),
           ylab=paste(labels[[j]], "[kg]"), ylim=c(0, maxComp[labels[[j]]]))
      legend("topleft", legend = c("obs", "pred"), col = c("red", "blue"), pch=c(19, 3))
      points(x=dat$BHD, y=pred$eq[[j]]$pred, pch=3, col="blue")
    }
  }
}
