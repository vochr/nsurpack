#' @title construct weight matrix
#' @description construct weight matrix for system of equations
#' @param obj list containing the diagonal elements ("weights") for the weight matrix
#' @param method = for which case should weight matrix be calculated, either
#' "OLS" or "SUR" is available and valid
#' @details no details
#' @return weightMatrix = elementwise squareroot operation on the inverse of the
#' weight matrix
#' @examples
#' 1+1
#' @export

buildWeightMatrix <- function(obj, method){

  require("Matrix")
  G <- length(obj)
  diagElem <- as.numeric()
  ## put weights of all eqns into one vector
  for(i in 1:G){
    # obj[[i]] contains final weights already; here we need the invers of it
    # diagElem <- c(diagElem, rep(1, length(obj[[i]])))
    ############################################################################
    ## the correct weights are calculated already in function 'calculateGNLSweights'
    ## and here they do not have to be modified!!!
    ## reason is:  the nlme book takes the inverse square root of the variance
    ## matrix Lambda, see p. 202.
    ############################################################################
    diagElem <- c(diagElem, 1/obj[[i]])
  }
  ## transform weights vector into diagonal matrix
  if(method == "OLS"){
    weightMatrix <- Diagonal(x=diagElem)
  } else if(method == "SUR"){
    ## Diagonal: function from package 'Matrix'
    weightMatrix <- sqrt(solve(Diagonal(x=diagElem)))
  }
  ## add weights as attribute to weightMatrix
  for(i in 1:G){
    attr(weightMatrix, paste0("weights:eqn", i)) <- obj[[i]]
  }
  return(weightMatrix)
}
