#' @title calculate degrees of freedom
#' @description calculate degrees of freedom to adjust the overall estimate
#'  @param m number of imputation chains
#'  @param lambda
#'  @param dfcom residual degree of freedom
#'  @param method character string, either "smallsample", or another string;
#'    in case of "smallsample" the  Barnard-Rubin adjusted degrees of freedom
#'    are calculated, else the conventional degrees of freedom, as in
#'    Rubin (1987)
#' @details function taken from package 'mice' v2.22
#' @return degrees of freedom to be used to calculate the number of standard
#' errors which are to be added/substracted to the overall estimate
#' @examples
#' 1+1
#' @export

mice.df <- function (m, lambda, dfcom, method)
{
  if (is.null(dfcom)) {
    dfcom <- 999999
    warning("Large sample assumed.")
  }
  lambda[lambda < 1e-04] <- 1e-04
  dfold <- (m - 1)/lambda^2
  dfobs <- (dfcom + 1)/(dfcom + 3) * dfcom * (1 - lambda)
  df <- dfold * dfobs/(dfold + dfobs)
  if (method != "smallsample")
    df <- dfold
  return(df)
}
