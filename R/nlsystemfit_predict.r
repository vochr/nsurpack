#' @title predict fit data and new data with nlsystemfit-object
#' @description Function to make predictions for a system of equation fit by SUR
#' Implementation according to Parresol (2001)
#' @param obj = a fitted object with function systemfit.sur()
#' @param newdata = dataframe with the same structure as used to fit the 'obj'
#' @param eqns = character vector for which predictions are desired; names
#' according to the response variables used to fit the 'obj'; default = "all"
#' @param se.fit = should standard errors be provided? default = FALSE
#' @param interval = character vector of type of interval desired? use
#' "confidence", "prediction" or both
#' @param level which tolerance / confidence level should be used? defaults to 0.95
#' @details no details, see Parresol (2001)
#' @return list with 3 dataframes: (1) meanPrediction, (2) Confidence Intervals and
#'  (3) Prediction Intervals for each system equation
#' @examples
#' ## see vignette
#' 1+1
#' @export

predict_sur <- function(obj, newdata = NULL, eqns = "all", se.fit = FALSE,
                        interval = "none", level = 0.95)

{
  if( missing(newdata) || is.null(newdata) ){
    stop("no newdata has been given")
  }

  if(length(interval) > 2){
    interval = "none"
    warning("'interval' too long! use either 'confidence', 'prediction' or both")
  } else {
    interval <- sapply(1:length(interval), function(a) {
      tf <- interval[a] %in% c("confidence", "prediction", "none")
      if(tf == FALSE) {
        warning(paste(interval[a], " is not valid element of 'interval'!",
                      "use either 'confidence', 'prediction' or both"))
        ""
      } else {
        interval[a]
      }
    })
  }

  EqNames <- sapply(1:obj$g, function(a) obj$eq[[a]]$eqnlabel)
  if("all" %in% eqns){
    eq <- 1:obj$g
    eqnames <- EqNames
  } else {
    eq  <- which(EqNames %in% eqns)
    eqnames <- EqNames[eq]
  }
  if( length(eq) < 1) stop("'eqns' not contained in fitted object")

  ## make parameters available
  for (i in 1:length(obj$b)) {
    name <- names(obj$b)[i]
    val <- obj$b[i]
    storage.mode(val) <- "double"
    assign(name, val)
  }

  if("prediction" %in% interval){
    ## calculate weights to be used in estimation of prediction interval
    Phi <- as.data.frame(matrix(0, nrow=nrow(newdata), ncol=length(eq)))
    colnames(Phi) <- eqnames
    for(i in seq(along=eq)){
      if(!is.null(obj$eq[[ eq[i] ]]$varPar)){
        tmp <- 1/extractWeight(obj = obj$eq[[ eq[i] ]]$varPar, data = newdata)
        attributes(tmp) <- NULL
        Phi[, i] <- tmp
      } else if( FALSE ) {
        ## here include code for Parresol (2001) approach...

      } else {
        Phi[, i] <- eval(obj$eq[[ eq[i] ]]$varModel)
      }
    }
    Phi <- unname(Phi)
  }


  ## data structures for results
  res <- list()
  res$eq <- list()
  resi <- list()
  ## prediction for each eqns
  for(i in seq(along=eq)){
    ## estimate mean prediction
    resi$eq <- eqnames[ i ]
    resi$pred <- with(newdata, as.vector(eval(as.formula(obj$eq[[ eq[i] ]]$formula)[[3]])))

    if("confidence" %in% interval | "prediction" %in% interval | se.fit == TRUE){
      ## create some necessary variables
      n <- obj$eq[[ eq[i] ]]$n
      k <- obj$eq[[ eq[i] ]]$k
      t <- qt(1-(1-level)/2, df = n-k)

      ## estimate confidence interval
      fb <- as.matrix(attr(with(newdata, with(as.list(obj$b),
                                              eval(deriv(obj$eq[[ eq[i] ]]$formula,
                                                         names(obj$b))))), "gradient"))
      estVar <- apply(fb, 1, function(a){t(a) %*% obj$covb %*% a})
      if(se.fit == TRUE){
        resi$se.fit <- sqrt(estVar)
      }
      if("confidence" %in% interval){
        temp <- t * sqrt(estVar)
        resi$lwrCI <- resi$pred - temp
        resi$CIval <- temp
        resi$uprCI <- resi$pred + temp
      }
      if("prediction" %in% interval){
        ## estimate prediction interval
        sii <- diag(obj$rcovest)[ eq[i] ] ## covariance of the errors for equation i
        w <- Phi[, i] ## weight for equation i
        ## attr(w_crown$modelStruct$varStruct, "formula")[[2]]
        temp <- t * sqrt(estVar + obj$sysvar * sii * w)
        resi$lwrPI <- resi$pred - temp
        resi$PIval <- temp
        resi$uprPI <- resi$pred + temp
      }
    }

    res$eq[[ i ]] <- resi
  }
  return(res)
}
