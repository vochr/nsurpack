#' @title pooling imputed nsur results
#' @description when fitting multiple nsur models on imputed data, pool the results
#' @param mira-object as result of the analysis of a mids-object
#' @param method character string, either "smallsample", or another string;
#' in case of "smallsample" the  Barnard-Rubin adjusted degrees of freedom
#' are calculated, else the conventional degrees of freedom, as in
#' Rubin (1987); see mice:::pool()
#' @param adjust According to the imputation logic, adjust the variance to
#' include the between imputation variance; adjusts only variance parameter.
#' Defaults to FALSE, i.e. no adjustment
#' @details function taken from package 'mice' v2.22; adjusted to work with
#' results from the NSUR-model; compare mice:::pool()
#' @return the pooled results of the mira-object; resulting class is 'mipo'
#' @examples
#' 1+1
#' @import mice
#' @export

pool.nsur <- function (object, method = "smallsample", adjust = F)
{
  call <- match.call()
  if (!is.mira(object))
    stop("The object must have class 'mira'")
  m <- length(object$analyses)
  fa <- getfit(object, 1)
  if (m == 1) {
    warning("Number of multiple imputations m=1. No pooling done.")
    return(fa)
  }
  analyses <- getfit(object)

  k <- length(fa$b) # number of coefficients
  names <- names(fa$b) # names of coefficients

  qhat <- matrix(NA, nrow = m, ncol = k, dimnames = list(1:m, names)) # estimates
  u <- array(NA, dim = c(m, k, k), dimnames = list(1:m, names, names)) # se
  for (i in 1:m) {
    fit <- analyses[[i]]
    qhat[i, ] <- fit$b # extract the parameter estimates
    ui <- fit$covb #vcov(fit) ##extract the variance-covariance matrix of parameter estimates
    ui <- expandvcov(qhat[i, ], ui)
    if (ncol(ui) != ncol(qhat)){
      stop("Different number of parameters: coef(fit): ",
           ncol(qhat), ", vcov(fit): ", ncol(ui))
    }
    u[i, , ] <- array(ui, dim = c(1, dim(ui)))
  }
  qbar <- apply(qhat, 2, mean) # mean of parameter estimates of all imputations
  ubar <- apply(u, c(2, 3), mean) # within imputation variance
  e <- qhat - matrix(qbar, nrow = m, ncol = k, byrow = TRUE) # (qhat - qbar)
  b <- (t(e) %*% e)/(m - 1) # between imputation variance
  t <- ubar + (1 + 1/m) * b # total variance
  r <- (1 + 1/m) * diag(b/ubar) # relative increase in variance due to nonresponse
  lambda <- (1 + 1/m) * diag(b/t) #Proportion of the variation attributable to the missing data: (b+b/m)/t
  dfcom <- fa$n - k # changed!
  df <- mice.df(m, lambda, dfcom, method)
  fmi <- (r + 2/(df + 3))/(r + 1) # fraction of missing information
  ## there are some further elements to be pooled:
  ## residual variance
  sigma_nsur <- sapply(1:m, function(a) object$analyses[[a]]$sysvar)
  sigma_nsur <- mean(sigma_nsur) + (m+1)/m *  1/(m+1) * sum((sigma_nsur - mean(sigma_nsur))^2)

  ## variance parameter
  varPar <- numeric(length(object$analyses[[1]]$eq))
  for(neq in 1:length(varPar)){
    wm <- numeric(m)
    for(nimp in 1:m){
      w <- try(attr(object$analyses[[nimp]]$weights, paste0("weights:eqn", neq)), silent=T)
      (p <- attr(w, "par"))
      if(length(p) == 1){
        wm[nimp] <- p[[1]]
      } else {
        (n <- sapply(unique(names(w)), function(a) length(w[names(w)==a])))
        if(adjust == T){
          (wm[nimp] <- weighted.mean(p, n) + (m+1)/m * var(p))
        } else {
          (wm[nimp] <- weighted.mean(p, n))
        }

      }
    }
    # either all or none should be 0
    if(adjust == T){
      (varPar[neq] <- ifelse(!all(wm==0), mean(wm, na.rm=T) + (m+1)/m * var(wm), NA))
    } else {
      (varPar[neq] <- ifelse(!all(wm==0), mean(wm, na.rm=T), NA))
    }

  }

  ## variance-covariance matrix of the errors
  g <- length(object$analyses[[1]]$eq) # number of equations
  namesE <- sapply(1:g, function(a) object$analyses[[1]]$eq[[a]]$eqnlabel)
  u <- array(NA, dim = c(m, g, g), dimnames = list(1:m, namesE, namesE))
  for (i in 1:m) {
    # i <- 1
    ui <- object$analyses[[i]]$rcovest
    u[i, , ] <- array(ui, dim = c(1, dim(ui)))
  }
  ubar <- apply(u, c(2, 3), "mean") # within imputation variance of the errors
  var_ubar <- apply(u, c(2, 3), FUN = "var") # between imputation variance of the errors
  # var uses denominator 'n-1' which is desired in this case!
  Sigma <- ubar + (1 + 1/m) * var_ubar # total variance

  ## additional elements for proper prediction function
  eqnlabel <- sapply(1:g, function(a) object$analyses[[1]]$eq[[a]]$eqnlabel) # response variable
  formula <- sapply(1:g, function(a) object$analyses[[1]]$eq[[a]]$formula) # model formula for each equation
  n <- sapply(1:g, function(a) object$analyses[[1]]$eq[[a]]$n) #number of obs in each equation
  k <- sapply(1:g, function(a) object$analyses[[1]]$eq[[a]]$k) #number of parameter in each equation
  varModel <- sapply(1:g, function(a) object$analyses[[1]]$eq[[a]]$varModel) #modelling variance

  ## create return object
  names(r) <- names(df) <- names(fmi) <- names(lambda) <- names
  names(varPar) <- names(n) <- names(k) <- names(varModel) <- namesE
  fit <- list(call = call, call1 = object$call, call2 = object$call1,
              nmis = object$nmis, m = m, qhat = qhat, u = u, qbar = qbar,
              ubar = ubar, b = b, t = t, r = r, dfcom = dfcom, df = df,
              fmi = fmi, lambda = lambda,
              sigma_nsur = sigma_nsur, varPar = varPar, Sigma = Sigma,
              g = g, k = k, n = n, eqnlabel = eqnlabel, formula = formula,
              varModel = varModel)
  oldClass(fit) <- c("mipo", oldClass(object))
  return(fit)
}
