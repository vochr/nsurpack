#' @title plot pooled estimated as distribution
#' @description this function plots the assumed pooled distribution of each
#' parameter of the regression into one plot. Additionally, the distribution of
#' each parameter from the separated fits of each imputation chain is added
#' @param fit a 'mira' object with fitted nlsystemfit results
#' @param ccResult the result of the complete case analysis
#' @details no details
#' @return side effects: plot
#' @examples
#' 1+1
#' @export

plot_par_dist <- function(fit, ccResult = NULL){

  ## load necessary functions
  # source(file.path(getwd(), "Programme/Eigenentwicklung/RCode/pool_functions.r"))

  ## adding of ccResult not implemented yet
  if(!is.null(ccResult)){
    ccResult <- NULL
    message("adding of ccResult not implemented yet")
  }

  ## check which equations are NOT summary compartments
  obj <- fit$analyses[[1]]
  use <- rep(F, length(obj$eq))
  for(i in 1:length(obj$eq)){
    vars <- all.vars(obj$eq[[i]]$formula)
    vars2 <- vars[grep(pattern = "[a][[:digit:]]", vars)]
    x <- sapply(strsplit(vars2, ""), function(x, n, m) tail(head(x, n), m), n=2, m=1)
    use[[i]] <- length(unique(x)) == 1
  }
  linecolor <- rainbow(length(fit$analyses))
  i <- unlist(as.vector(sapply(c(1:(length(fit$analyses[[1]]$eq)-1))[use],
                               function(a) rep(a, length(fit$analyses[[1]]$eq[[a]]$b)) )))
  eqn <- paste( sapply(i, function(a) fit$analyses[[1]]$eq[[a]]$formula))#,
  cc <- ifelse(is.null(ccResult), FALSE, TRUE) # add completeCase results to plots
  if(cc==TRUE){
    #     load(file.path(getwd(), "Daten/aufbereiteteDaten/RData/CompleteCase/NSURTacc.RData"))
    #     ccEst <- fit.sur$b
    #     ccSE <- fit.sur$se
  }
  res <- pool.nsur(fit)
  pEst <- summary(res)[, "est"] # extract the pooled results via summary object
  pSE <- summary(res)[, "se"] # extract the pooled results via summary object
  for(i in 1:length(fit$analyses[[1]]$b)){ # for each parameter
    est <- sapply(1:length(fit$analyses), function(a) fit$analyses[[a]]$b[i])
    se <- sapply(1:length(fit$analyses), function(a) fit$analyses[[a]]$se[i])
    if(cc==TRUE){
      xmin <- ccEst[i] - 4*ccSE[i]
      xmax <- ccEst[i] + 4*ccSE[i]
      ymax <- dnorm(ccEst[i], mean = ccEst[i], sd = ccSE[i])
    } else {
      xmin=100; xmax=-100; ymax=0
    }
    xmin <- min(pEst[i]-4*pSE[i], xmin)
    xmax <- max(pEst[i]+4*pSE[i], xmax)
    ymax <- max(dnorm(est, mean = est, sd = se), ymax)
    plot(NULL, xlim=c(xmin, xmax), ylim=c(0,ymax), ylab="", xlab="Parameterwert",
         main=paste0("Parameter ", names(est[1]), ": ", eqn[i]))
    curve(dnorm(x = x, pEst[i], pSE[i]), from=xmin, to=xmax, n= 500, type="l",
          add=T, lwd = 2, col="blue")
    lines(x = c(pEst[i], pEst[i]), y = c(0, dnorm(pEst[i], pEst[i], pSE[i])),
          col="blue", lwd=2, lty=1)
    if(cc==TRUE){ # add elements for complete Case Analysis
      legend("topleft", legend = c(paste("mids", 1:10), "pooled", "Complete Case"),
             lwd = c(rep(1, 10), 2, 2), lty = c(rep(2, 10), 1, 1),
             col=c(linecolor, "blue", "red"))
      curve(dnorm(x = x, ccEst[i], ccSE[i]), from=xmin, to=xmax, n= 500, type="l",
            add=T, lwd = 2, col="red")
      lines(x = c(ccEst[i], ccEst[i]), y = c(0, dnorm(ccEst[i], ccEst[i], ccSE[i])),
            col="red", lwd=2, lty=1)
    } else {
      legend("topleft", legend = c(paste("mids", 1:10), "pooled"), lwd = c(rep(1, 10), 2),
             lty = c(rep(2, 10), 1), col=c(linecolor, "blue"))
    }

    for(j in 1:length(est)){ # for each imputed dataset
      curve(dnorm(x = x, est[j], se[j]), from=xmin, to=xmax, n= 500, type="l", add=T)
      lines(x = c(est[j], est[j]), y = c(0, dnorm(est[j], est[j], se[j])),
            col=linecolor[j], lwd=1, lty=2)
    }
  }
}
