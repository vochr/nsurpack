#' @title create weighted error covariance matrix
#' @description calculate the weighted error covariance matrix from the weighted
#' residuals of nls models and the weights matrix Delta
#' @param Resid list with weighted residuals for each system equation
#' @param Delta matrix containing the weights, see details
#' @details
#' calculate the weighted error covariance matrix (cf. Parresol 2001, eq. 21)
#' from the weighted residuals of nls models and the weights matrix Delta
#' Delta is an elementwise square root operation on the inverse of the weight
#' matrix [Phi]', cf. Parresol (2001, p. 871))
#' @return invertible weighted error covariance matrix; if not invertible, NULL
#' @examples
#' 1+1
#' @export
#'
#'
buildSigma <- function(Resid, Delta){

  G <- length(Resid)
  delta <- list()
  df <- array(0, c(G))
  beg <- array(0, c(G))
  end <- array(0, c(G))
  #Delta is symmetric and keepts the same number of elements for each if the G equations
  n <- nrow(Delta) / G
  for (i in 1:G) {

    df[i] <- attr(Resid[[i]], "df")
    beg[i] <- (i-1) * n + 1
    end[i] <- i * n

    delta[[i]] <- Delta[ beg[i]:end[i], beg[i]:end[i] ]
  }

  S <- Matrix(0, G, G)
  for (i in 1:G) {
    for (j in 1:G) {
      S[i, j] <- 1 / (sqrt((df[i]) * (df[j]))) *
        t(Resid[[i]]) %*% (t(delta[[i]]) %*% delta[[j]]) %*% Resid[[j]]
    }
  }

  ## if not of full rank, stop!
  if(rankMatrix(S, method = "qr") == G){ ## TRUE --> full rank!
    return(S)
  } else {
    stop("S (weighted error covariance matrix) not of full rank!")
    return(NULL)
  }
}
