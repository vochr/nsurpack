#' @title fitting nonlinear SUR-system
#' @description code from systemfit-package, extended to serve my needs
#' @param method must be "SUR"
#' @param eqns list with formulas for each system equation
#' @param univGNLS the gnls-objects used to fit the univariate case; these are
#' used to extract the starting values (from the last object), parameter names
#'    and the weights used
#' @param data data.frame containing the data to fit the SUR
#' @param Delta weights
#' @param S Sigma, error covariance-matrix
#' @param opt.fn optimised used (nlm, optim, nlminb, nls.LM)
#' @param opt.method only relevant if \code{opt.fn} is "optim", see there.
#' @param nlm.gradtol only relevant if \code{opt.fn} is "nlm", see there.
#' @param pl print level, only relevant if \code{opt.fn} is "nlm", see there.
#' @param useHessian in optimisation, i.e. in function \code{wknls}
#' @param eqnlabels number of equation; used in the result as reference
#' @param solvtol tolerance for detecting linear dependencies in matrixes ('qr()')
#' @param maxiter maximum number of iterations for the nlm function
#' @details Function to implement a 'seemingly unrelated regression' on gnls
#' objects. This code is taken from the systemfit-package and somewhat modified.
#' Especially, reduced to the SUR-case and extended for using weights. These
#' weights and also the starting values? are extracted from the gnls-objects.
#' @return a list with results for the whole system including results for each
#' equation. Compare with the original function 'nlsystemfit'.
#' @examples
#' 1+1
#' @import Matrix
#' @export

nlsystemfit.sur <- function (method = "SUR", eqns, startvals, data = parent.frame(),
                             Delta = NULL, S = NULL, opt.fn = "nlm", opt.method = NULL,
                             nlm.gradtol = 1e-6, pl=0, useHessian = FALSE,
                             eqnlabels = c(as.character(1:length(eqns))),
                             solvtol = .Machine$double.eps, maxiter = 10000, ...)

{
  if(!is.data.frame(data)){
    stop("data must be a data.frame")
  }
  require("Matrix")
  solvtol = .Machine$double.eps

  if (!(method == "SUR")) {
    stop("The method must be 'SUR'")
  }
  if(is.null(S)){
    stop("SUR-Model needs error covariance-matrix S!")
  }
  if(is.null(Delta)){
    warning("no weights-applied to SUR-Model!")
  }

  lhs <- list()
  rhs <- list()
  derivs <- list()
  results <- list()
  results$eq <- list()
  resulti <- list()
  residi <- list()
  G <- length(eqns)
  n <- array(0, c(G))
  k <- array(0, c(G))
  df <- array(0, c(G))
  ssr <- array(0, c(G))
  mse <- array(0, c(G))
  rmse <- array(0, c(G))
  r2 <- array(0, c(G))
  adjr2 <- array(0, c(G))
  nobs <- dim(data)[[1]]
  X <- array()
  resids <- array()
  resids <- NULL
  attach(data)
  if (method == "SUR") {

    Sinv <- try(solve(S, tol = solvtol) %x% Diagonal(nobs), silent=TRUE)
#     if(class(Sinv) == "try-error") Sinv <- ginv(S) %x% diag(nobs)
    if(opt.fn == "nlm"){
      (est <- nlm(wknls, startvals, print.level=pl, typsize = abs(startvals),
                  iterlim = maxiter, eqns = eqns, data = data, gradtol = nlm.gradtol,
                  fitmethod = method, parmnames = startvals, S = Sinv, W = Delta,
                  useHessian = useHessian))
      estimate <- est$estimate
    } else if(opt.fn == "optim"){
      opt.method <- ifelse(c("BFGS", "CG", "L-BFGS-B") %in% opt.method, opt.method, "BFGS")[1]
      (est <- optim(startvals, fn = wknls, gr = attr(wknls, "gradient"),
                      control = list(maxit = 100, trace = 1), method = opt.method,
                      eqns = eqns, data = data,
                      fitmethod = method, parmnames = startvals, S = Sinv, W = Delta))
      estimate <- est$par
    } else if(opt.fn == "nlminb"){
      (est <- nlminb(startvals, wknls, gradient = attr(wknls, "gradient"), hessian = attr(wknls, "hessian"),
                      control = list(iter.max = maxiter, trace = 5),
                      eqns = eqns, data = data, fitmethod = method,
                      parmnames = startvals, S = Sinv, W = Delta))
      estimate <- est$par
    } else if(opt.fn == "nls.LM"){
      ## nls.lm() from minpack.lm requires residuals to be returned from obj-fn
      (est <- nls.lm(par = startvals, fn = fn_wknls, jac = jac_wknls, eqns = eqns,
                       data = data, fitmethod = "SUR", parmnames = startvals,
                       S = Sinv, W = Delta, control=nls.lm.control(nprint=1)))
      estimate <- est$par
    }
  }

  names(estimate) <- names(startvals)
  for (i in 1:length(estimate)) {
    name <- names(estimate)[i]
    assign(name, NULL)
    val <- estimate[i]
    storage.mode(val) <- "double"
    assign(name, val)
  }
  X <- NULL
  results$resids <- array()
  results$resids <- NULL

  for (i in 1:G) {
    lhs[[i]] <- as.matrix(eval(as.formula(eqns[[i]])[[2]]))
    rhs[[i]] <- as.matrix(eval(as.formula(eqns[[i]])[[3]]))
    residi[[i]] <- lhs[[i]] - rhs[[i]]
    derivs[[i]] <- deriv(as.formula(eqns[[i]]), names(startvals))
    jacobian <- attr(eval(derivs[[i]]), "gradient")
    n[i] <- length(lhs[[i]])
    k[i] <- qr(jacobian)$rank
    df[i] <- n[i] - k[i]
    if(!is.null(Delta)){
      w <- attr(Delta, paste0("weights:eqn", i))
    } else {
      w <- 1
    }
    ssr[i] <- sum((w * as.vector(residi[[i]]))^2)
    mse[i] <- ssr[i]/(n[i] - k[i])
    rmse[i] <- sqrt(mse[i])
    X <- rbind(X, jacobian)
    results$resids <- cbind(results$resids, as.matrix(residi[[i]]))
  }
  print(estimate)
  if (method == "SUR") {
    covb <- qr.solve(t(X) %*% t(Delta) %*% Sinv %*% Delta %*% X, tol = solvtol)
  }
  colnames(covb) <- rownames(covb)
  se2 <- sqrt(diag(covb)) ## standard error of the estimated error variance
  t.val <- estimate/se2 ## t-statistic: estimate devided by its standard error
  prob <- 2 * (1 - pt(abs(t.val), sum(n) - sum(k))) ## t.val compared to t-distribution (twosided) --> p
  results$method <- method
  results$n <- sum(n)
  results$k <- sum(k)
  results$b <- estimate
  results$se <- se2
  results$t <- t.val
  results$p <- prob
  for (i in 1:G) {
    eqn.terms <- vector()
    eqn.est <- vector()
    eqn.se <- vector()
    jacob <- attr(eval(deriv(as.formula(eqns[[i]]), names(startvals))),
                  "gradient")
    for (v in 1:length(estimate)) {
      if (qr(jacob[, v])$rank > 0) {
        eqn.terms <- rbind(eqn.terms, name <- names(estimate)[v])
        eqn.est <- rbind(eqn.est, estimate[v])
        eqn.se <- rbind(eqn.se, se2[v])
      }
    }
    resulti$method <- method
    resulti$i <- i
    resulti$eqnlabel <- eqnlabels[[i]]
    resulti$formula <- eqns[[i]]
    resulti$varModel <- attr(attr(Delta, paste0("weights:eqn", i)), "formula")
    resulti$varPar <- attr(attr(Delta, paste0("weights:eqn", i)), "par")
    resulti$b <- as.vector(eqn.est)
    names(resulti$b) <- eqn.terms
    resulti$se <- eqn.se
    resulti$t <- resulti$b/resulti$se
    resulti$p <- 2 * (1 - pt(abs(resulti$t), n[i] - k[i]))
    resulti$n <- n[i]
    resulti$k <- k[i]
    resulti$df <- df[i]
    resulti$predicted <- rhs[[i]]
    resulti$residuals <- residi[[i]]
    resulti$ssr <- ssr[i] # sum of squared residuals
    resulti$mse <- mse[i] # mean squared error: ssr / (n - k)
    resulti$s2 <- mse[i] # if residuals are unbiased, mse == variance, else mse == var + bias^2
    resulti$rmse <- rmse[i] # root mean squared error: sqrt(ssr)
    resulti$s <- rmse[i]  # if residuals are unbiased, rmse == sqrt(variance) == stdev
    coefNames <- rownames(covb)[rownames(covb) %in% strsplit(as.character(eqns[[i]])[3],
                                                             "[^a-zA-Z0-9.]")[[1]]]
    resulti$covb <- covb[coefNames, coefNames]
    resulti$r2 <- 1 - ssr[i]/((crossprod(lhs[[i]])) - mean(lhs[[i]])^2 * nobs) ## same as FI in Parresol (2001)
    resulti$adjr2 <- 1 - ((n[i] - 1)/df[i]) * (1 - resulti$r2)
    class(resulti) <- "nlsystemfit.equation"
    results$eq[[i]] <- resulti
  }
  results$solvtol <- solvtol
  results$covb <- covb
  results$rcov <- S
  results$rcor <- cor(results$resids)
  results$drcov <- det(results$rcov)
  if (method == "2SLS" || method == "3SLS") {
  }
  if (method == "SUR" || method == "3SLS" || method == "GMM") {
    results$rcovest <- S # we don't update S so initial and final S is identical
  }
  results$weights <- Delta
  results$method <- method
  results$g <- G
  results$nlmest <- est
  if(opt.fn == "nlm"){
    systemvar <- est$minimum / ( sum(n) - length(estimate) ) ## cf. Parresol (2001, eq. 26)
    convergence <- est$code
    if(convergence >= 4){
      message <- "Optimization did not converge"
    } else {
      message <- paste("Convergence with code", convergence)
    }
  } else if(opt.fn == "optim"){
    systemvar <- est$value / ( sum(n) - length(estimate) ) ## cf. Parresol (2001, eq. 26)
    convergence <- est$convergence
    message <- est$message
  } else if(opt.fn == "nlminb"){
    systemvar <- est$objective / ( sum(n) - length(estimate) ) ## cf. Parresol (2001, eq. 26)
    convergence <- est$convergence
    message <- est$message
  }
  results$sysvar <- systemvar
  class(results) <- "nlsystemfit.system"
  detach(data)
  if (convergence != 0) {
    warning(message)
  }
  results
}

