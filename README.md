Fitting a NSUR model
====================

This vignette shows how to fit a nonlinear seemingly unrelated
regression (NSUR) as proposed by Parresol (2001) and the data provided
therein.

The NSUR model fits multiple regression at once, optimising the errors
of each model at the same time, considering them ‘seemingly unrelated’
though the relation is explicitly included by a error covariance matrix.

Lets start checking the data of Parresol (2001).

    require(NSURpack)
    data("SlashPine")
    head(SlashPine)

    ##   TreeNo   D    H LCL Age Wood Bark Crown Tree
    ## 1      1 5.6  7.9 2.1  21  6.5  2.3   1.0  9.8
    ## 2      2 6.4  8.5 1.2  21  7.4  2.6   2.1 12.1
    ## 3      3 8.1 10.7 2.7  20 17.6  4.5   2.3 24.4
    ## 4      4 8.4 11.3 3.4  21 18.5  4.3   4.2 27.0
    ## 5      5 9.1 11.0 4.3  21 22.6  5.4   5.6 33.6
    ## 6      6 9.9 13.1 3.4  21 30.6  7.4   5.5 43.5

univariate Models
-----------------

Following Parresol (2001), we fit univariate models using , and check
his error modelling. Finally, we compare our results to those in his
paper.

### Wood: W ~ (b11 \* (D^2 \* H)^b12)

    ## Wood
    ## unweighted model
    nls_wood <- nls(Wood ~ b11 * (D^2 * H)^b12, data=SlashPine, start = c(b11=0.1, b12=2))
    plot(log(resid(nls_wood)^2) ~ log(SlashPine$D^2 * SlashPine$H))
    abline(m <- lm(log(resid(nls_wood)^2) ~ log(SlashPine$D^2 * SlashPine$H)))

<img src="fit_Parresol2001_NSUR_files/figure-markdown_strict/unnamed-chunk-2-1.png" style="display: block; margin: auto;" />

    coef(m)[[2]] # identical to result of Parresol (2001, Tab.2): 2.114

    ## [1] 2.113576

    ## weighted model
    nls_wood_w <- nls(Wood ~ b11 * (D^2 * H)^b12, data=SlashPine, start = c(b11=0.1, b12=2),
                      weights = 1 / (D^2 * H)^coef(m)[[2]])
    summary(nls_wood_w)

    ## 
    ## Formula: Wood ~ b11 * (D^2 * H)^b12
    ## 
    ## Parameters:
    ##     Estimate Std. Error t value Pr(>|t|)    
    ## b11 0.016363   0.001817   9.005 5.77e-11 ***
    ## b12 1.058498   0.013094  80.839  < 2e-16 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.001534 on 38 degrees of freedom
    ## 
    ## Number of iterations to convergence: 20 
    ## Achieved convergence tolerance: 1.94e-07

    coef(nls_wood_w) # Parresol (2001, Tab.2): 0.016363, 1.0585

    ##        b11        b12 
    ## 0.01636288 1.05849779

    res_wood_w <- resid(nls_wood_w) # keeping the weighted residuals
    attr(res_wood_w, "df") <- df.residual(nls_wood_w) # and the degrees of freedom
    summary(nls_wood_w)$sigma^2 # Parresol (2001, tab.2): 2.3446e-06

    ## [1] 2.352958e-06

The results of the paper can fully be reconstructed.

### Bark B ~ (b21 \* D^b22)

    ## Bark
    ## unweighted model
    nls_bark <- nls(Bark ~ b21 * D^b22, data=SlashPine, start = c(b21=0.1, b22=2))
    plot(log(resid(nls_bark)^2) ~ log(SlashPine$D))
    abline(m <- lm(log(resid(nls_bark)^2) ~ log(SlashPine$D)))

<img src="fit_Parresol2001_NSUR_files/figure-markdown_strict/unnamed-chunk-3-1.png" style="display: block; margin: auto;" />

    coef(m)[[2]] # identical to result of Parresol (2001, Tab.2): 3.715

    ## [1] 3.715056

    ## weighted model
    nls_bark_w <- nls(Bark ~ b21 * D^b22, data=SlashPine, start = c(b21=0.1, b22=2),
                      weights = 1/D^coef(m)[[2]])
    summary(nls_bark_w)

    ## 
    ## Formula: Bark ~ b21 * D^b22
    ## 
    ## Parameters:
    ##     Estimate Std. Error t value Pr(>|t|)    
    ## b21 0.046277   0.007079   6.537 1.05e-07 ***
    ## b22 2.209319   0.052160  42.356  < 2e-16 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.01631 on 38 degrees of freedom
    ## 
    ## Number of iterations to convergence: 5 
    ## Achieved convergence tolerance: 5.16e-06

    coef(nls_bark_w) # Parresol (2001, Tab.2): 0.046277, 2.2093

    ##        b21        b22 
    ## 0.04627671 2.20931932

    res_bark_w <- resid(nls_bark_w) # keeping the weighted residuals
    attr(res_bark_w, "df") <- df.residual(nls_bark_w) # and the degrees of freedom
    summary(nls_bark_w)$sigma^2 # Parresol (2001, tab.2): 2.6609e-04

    ## [1] 0.0002660431

### Crown C ~ (b31 \* D^b32 \* H^b33)

    ## Crown
    ## unweighted model
    nls_crown <- nls(Crown ~ b31 * D^b32 * H^b33, data=SlashPine, start = c(b31=0.1, b32=2, b33=1))
    plot(log(resid(nls_crown)^2) ~ log(SlashPine$D))
    abline(m <- lm(log(resid(nls_crown)^2) ~ log(SlashPine$D) + (I(SlashPine$H^2))))

    ## Warning in abline(m <- lm(log(resid(nls_crown)^2) ~ log(SlashPine$D) +
    ## (I(SlashPine$H^2)))): only using the first two of 3 regression coefficients

<img src="fit_Parresol2001_NSUR_files/figure-markdown_strict/unnamed-chunk-4-1.png" style="display: block; margin: auto;" />

    coef(m)[2:3] # identical to result of Parresol (2001, Tab.2): 7.322, -0.006185

    ## log(SlashPine$D) I(SlashPine$H^2) 
    ##      7.321945032     -0.006184469

    ## weighted model
    nls_crown_w <- nls(Crown ~ b31 * D^b32 * H^b33, data=SlashPine, start = c(b31=0.1, b32=2, b33=1),
                      weights = 1/(D^coef(m)[[2]] * exp(coef(m)[[3]] * H^2)))

    summary(nls_crown_w)

    ## 
    ## Formula: Crown ~ b31 * D^b32 * H^b33
    ## 
    ## Parameters:
    ##     Estimate Std. Error t value Pr(>|t|)    
    ## b31  0.02738    0.01284   2.132  0.03973 *  
    ## b32  3.68042    0.25731  14.303  < 2e-16 ***
    ## b33 -1.26240    0.37458  -3.370  0.00177 ** 
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.0006278 on 37 degrees of freedom
    ## 
    ## Number of iterations to convergence: 8 
    ## Achieved convergence tolerance: 3.399e-06

    coef(nls_crown_w) # Parresol (2001, Tab.2): 0.027378, 3.6804, -1.2624

    ##         b31         b32         b33 
    ##  0.02737884  3.68041872 -1.26240461

    res_crown_w <- resid(nls_crown_w) # keeping the weighted residuals
    attr(res_crown_w, "df") <- df.residual(nls_crown_w) # and the degrees of freedom
    summary(nls_crown_w)$sigma^2 # Parresol (2001, tab.2): 3.9417e-07

    ## [1] 3.941534e-07

### Tree T ~ (b11 \* (D^2 \* H)^b12) + (b21 \* D^b22) + (b31 \* D^b32 \* H^b33)

We don’t need a separate unweighted regression model for the total
aboveground biomass, instead, we use the residuals of the unweighted
component models.

    ## Tree
    res_tree <- resid(nls_wood) + resid(nls_bark) + resid(nls_crown)
    plot(log(res_tree^2) ~ log(SlashPine$D * SlashPine$H))
    abline(m <- lm(log(res_tree^2) ~ log(SlashPine$D * SlashPine$H)))

<img src="fit_Parresol2001_NSUR_files/figure-markdown_strict/unnamed-chunk-5-1.png" style="display: block; margin: auto;" />

    coef(m)[[2]] # identical to result of Parresol (2001, eq. 19): 3.450

    ## [1] 3.450689

    w_tree <- 1/(SlashPine$D * SlashPine$H)^coef(m)[[2]] # calculate weights
    res_tree_w <- resid(nls_wood_w) + resid(nls_bark_w) + resid(nls_crown_w) # residuals
    attr(res_tree_w, "df") <- 40 - 7 # degrees of freedom

All parameter estimates are pretty much ok; residual variance (sigma^2)
slightly differs from the results reported by Parresol (2001).

system of equations: NSUR
-------------------------

    ## setting up the NSUR-Model
    ## modeling-functions
    wood_func <- Wood ~ b11 * (D^2 * H)^b12
    bark_func <- Bark ~ b21 * D^b22
    crown_func <- Crown ~ b31 * D^b32 * H^b33
    tree_func <- Tree ~ (b11 * (D^2 * H)^b12) + (b21 * D^b22) + (b31 * D^b32 * H^b33)
    model <- list(wood_func, bark_func, crown_func, tree_func)
    labels <- sapply(model, function(m){as.character(m[[2]])})
    ## start values
    startvals <- c(coef(nls_wood_w), coef(nls_bark_w), coef(nls_crown_w))
    ## weights matrix (p. 871, left column, far down)
    weightsM <- list(weights(nls_wood_w), weights(nls_bark_w), weights(nls_crown_w), w_tree)
    Delta <- buildWeightMatrix(weightsM, method = "SUR") #Weight-Matrix

    ## Loading required package: Matrix

    ## build error covariance matrix
    resM <- list(res_wood_w, res_bark_w, res_crown_w, res_tree_w)
    S <- buildSigma(resM, Delta) 
    round(cov2cor(S), 4)

    ## 4 x 4 sparse Matrix of class "dsCMatrix"
    ##                                   
    ## [1,]  1.0000 0.2828 -0.1154 0.8396
    ## [2,]  0.2828 1.0000  0.0127 0.4736
    ## [3,] -0.1154 0.0127  1.0000 0.3469
    ## [4,]  0.8396 0.4736  0.3469 1.0000

Now, having all parts of the model specified, we can optimise the
regression parameters using the functions implemented in this package.

    fit.sur <- nlsystemfit.sur(method = "SUR", eqns = model, #startvals = startvals,
                               startvals = c(b11=0.01, b12=1, b21=0.01, b22=2, b31=0.01, b32=2, b33=-1),
                               Delta = Delta, S = S, eqnlabels = labels, pl=1,
                               data=SlashPine)

    ## iteration = 0
    ## Step:
    ## [1] 0 0 0 0 0 0 0
    ## Parameter:
    ## [1]  0.01  1.00  0.01  2.00  0.01  2.00 -1.00
    ## Function Value
    ## [1] 6561.23
    ## Gradient:
    ## [1] -286689.48065  -24571.40073  -59671.37928   -1718.89845   -2059.74719
    ## [6]     -56.26322     -57.81934

    ## Warning in nlm(wknls, startvals, print.level = pl, typsize =
    ## abs(startvals), : NA/Inf durch größte positive Zahl ersetzt

    ## Warning in nlm(wknls, startvals, print.level = pl, typsize =
    ## abs(startvals), : NA/Inf durch größte positive Zahl ersetzt

    ## iteration = 92
    ## Parameter:
    ## [1]  0.01624649  1.05896293  0.04606414  2.21106146  0.01401545  3.51005364
    ## [7] -0.87211409
    ## Function Value
    ## [1] 142.7094
    ## Gradient:
    ## [1] -4.166536e-03 -5.753406e-04  1.521359e-03  1.639676e-04  1.731333e-03
    ## [6]  7.400794e-05  8.143460e-05
    ## 
    ## Aufeinanderfolgende Iterationen innerhalb der Toleranz.
    ## Aktuelle Iteration ist wahrscheinlich Lösung.
    ## 
    ##         b11         b12         b21         b22         b31         b32 
    ##  0.01624649  1.05896293  0.04606414  2.21106146  0.01401545  3.51005364 
    ##         b33 
    ## -0.87211409

    ## Warning in nlsystemfit.sur(method = "SUR", eqns = model, startvals = c(b11
    ## = 0.01, : Convergence with code 2

Convergence code=2 means “successive iterates within tolerance, current
iterate is probably solution”.

We can compare to the results presented in Parresol (2001):

    rbind(fit.sur$b, 
          c(0.016245, 1.0590, 0.046040, 2.2112, 0.014015, 3.5098, -0.87190))

    ##             b11      b12        b21      b22        b31      b32
    ## [1,] 0.01624649 1.058963 0.04606414 2.211061 0.01401545 3.510054
    ## [2,] 0.01624500 1.059000 0.04604000 2.211200 0.01401500 3.509800
    ##             b33
    ## [1,] -0.8721141
    ## [2,] -0.8719000

also the estimated standard errors are almost identical:

    rbind(fit.sur$se, 
          c(0.00172, 0.0124, 0.00530, 0.0397, 0.00436, 0.147, 0.193))

    ##              b11        b12         b21      b22         b31       b32
    ## [1,] 0.001715414 0.01244357 0.005294107 0.039654 0.004358363 0.1473236
    ## [2,] 0.001720000 0.01240000 0.005300000 0.039700 0.004360000 0.1470000
    ##            b33
    ## [1,] 0.1934864
    ## [2,] 0.1930000

Checking the estimated variance-covariance matrix of the parameters
(check against p. 873, bottom):

    options(digits = 3, scipen = -2)
    fit.sur$covb

    ##           b11       b12       b21       b22       b31       b32       b33
    ## b11  2.94e-06 -2.12e-05  1.93e-06 -1.41e-05 -1.54e-06  1.83e-05  1.99e-05
    ## b12 -2.12e-05  1.55e-04 -1.35e-05  1.01e-04  1.14e-05 -1.04e-04 -1.82e-04
    ## b21  1.93e-06 -1.35e-05  2.80e-05 -2.07e-04 -3.66e-06  1.70e-05  7.31e-05
    ## b22 -1.41e-05  1.01e-04 -2.07e-04  1.57e-03  3.00e-05 -4.80e-05 -6.98e-04
    ## b31 -1.54e-06  1.14e-05 -3.66e-06  3.00e-05  1.90e-05  8.20e-05 -5.31e-04
    ## b32  1.83e-05 -1.04e-04  1.70e-05 -4.80e-05  8.20e-05  2.17e-02 -2.42e-02
    ## b33  1.99e-05 -1.82e-04  7.31e-05 -6.98e-04 -5.31e-04 -2.42e-02  3.74e-02

    options(digits = 7, scipen = 0)
    fit.sur$sysvar # compare to 0.933 given on page 874, middle part

    ## [1] 0.9327415

prediction and confidence interval
==================================

    predict_sur(obj = fit.sur, newdata = data.frame(D=20, H=17), eqns = "Tree", se.fit = FALSE,
                            interval = "confidence", level = 0.95)

    ## $eq
    ## $eq[[1]]
    ## $eq[[1]]$eq
    ## [1] "Tree"
    ## 
    ## $eq[[1]]$pred
    ## [1] 264.2321
    ## 
    ## $eq[[1]]$lwrCI
    ## [1] 257.8741
    ## 
    ## $eq[[1]]$CIval
    ## [1] 6.357965
    ## 
    ## $eq[[1]]$uprCI
    ## [1] 270.59

NB: prediction intervals cannot be calculated using the function,
because it is tight to close to the gnls approach (i.e. estimating
variance and covariance using nlme::gnls) followed in Vonderach et
al. (2018).

plotting results
----------------

    SlashPine$BHD <- SlashPine$D
    plot_sur(obj = fit.sur, data = SlashPine, lmfor = F)

![](fit_Parresol2001_NSUR_files/figure-markdown_strict/unnamed-chunk-12-1.png)![](fit_Parresol2001_NSUR_files/figure-markdown_strict/unnamed-chunk-12-2.png)![](fit_Parresol2001_NSUR_files/figure-markdown_strict/unnamed-chunk-12-3.png)![](fit_Parresol2001_NSUR_files/figure-markdown_strict/unnamed-chunk-12-4.png)![](fit_Parresol2001_NSUR_files/figure-markdown_strict/unnamed-chunk-12-5.png)![](fit_Parresol2001_NSUR_files/figure-markdown_strict/unnamed-chunk-12-6.png)![](fit_Parresol2001_NSUR_files/figure-markdown_strict/unnamed-chunk-12-7.png)

Reference
=========

Parresol, B. R. (2001). “Additivity of nonlinear biomass equations.”
Canadian Journal of Forest Research-Revue Canadienne De Recherche
Forestiere 31(5): 865-878.
